import java.util.List;

public class QuadraticEquationComparator {
    private QuadraticEquation q;

    public QuadraticEquationComparator(QuadraticEquation q) {
        this.q = q;
    }

    public double getMajorRoot() throws Exception {
        List<Double> roots = q.roots();
        if(roots.size() == 2){
            return Math.max(roots.get(0), roots.get(1));
        }
        else if(roots.size() == 1){
            return roots.get(0);
        }
        else throw new Exception("No roots");
    }
}
