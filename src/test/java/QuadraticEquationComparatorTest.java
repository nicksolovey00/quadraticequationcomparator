import org.junit.Assert;
import org.junit.Test;

public class QuadraticEquationComparatorTest {
    @Test
    public void testGetMajorRoot() throws Exception {
        QuadraticEquationComparator q1 = new QuadraticEquationComparator(new QuadraticEquation(1, -5, 4));
        QuadraticEquationComparator q2 = new QuadraticEquationComparator(new QuadraticEquation(1, -13, 12));
        QuadraticEquationComparator q3 = new QuadraticEquationComparator(new QuadraticEquation(1, -7, 12));

        Assert.assertEquals(4.0, q1.getMajorRoot(), 1e-10);
        Assert.assertEquals(12.0, q2.getMajorRoot(), 1e-10);
        Assert.assertEquals(4.0, q3.getMajorRoot(), 1e-10);
    }

    @Test(expected = Exception.class)
    public void testGetMajorRootWithException() throws Exception {
        QuadraticEquationComparator q3 = new QuadraticEquationComparator(new QuadraticEquation(1, 1, 12));
        q3.getMajorRoot();
    }
}
